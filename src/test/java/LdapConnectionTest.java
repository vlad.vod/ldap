import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class LdapConnectionTest {

    private LdapConnection connection;

    @Before
    public void setUp() {
        connection = new LdapNetworkConnection("localhost", 10389);
    }

    @Test
    public void testAnonymousBindRequest() throws Exception {
        connection.bind();
    }

    @Test
    public void testSimpleBindRequest() throws Exception
    {
        connection.bind( "uid=admin,ou=system", "secret" );
    }

    @Test
    public void testRebind() throws Exception
    {
        connection.bind( "cn=Joe,ou=people,dc=delta,dc=com", "secret" );
        assertTrue( connection.isConnected() );
        assertTrue( connection.isAuthenticated() );

        connection.bind( "cn=Kate,ou=people,dc=delta,dc=com", "secret" );
        assertTrue( connection.isConnected() );
        assertTrue( connection.isAuthenticated() );

        connection.bind( "cn=Mike,ou=people,dc=delta,dc=com", "secret" );
        assertTrue( connection.isConnected() );
        assertTrue( connection.isAuthenticated() );
    }
}
