import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;

public class LdapClient {

    private LdapConnection getConnection(){
        return new LdapNetworkConnection( "localhost", 10389 );
    }

    private void doBind(){

        try {
            getConnection().bind("ou=example, dc=com", "secret" );
            System.out.println("Bind successful");
        } catch (LdapException e) {
            e.printStackTrace();
            System.out.println("Bind failed");
        }
    }
}
